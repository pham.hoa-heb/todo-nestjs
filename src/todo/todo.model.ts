import { Field, ObjectType } from '@nestjs/graphql';
import { Model } from 'objection';

@ObjectType()
export class TodoModel extends Model {
  static tableName = 'todos';

  @Field()
  id: number;

  @Field()
  name: string;

  @Field()
  done: boolean;
}
