import { TodoDto } from './todo.dto';
import { TodoModel } from './todo.model';
import { Inject, Injectable, NotFoundException } from '@nestjs/common';

@Injectable()
export class TodoService {
  constructor(
    @Inject(TodoModel)
    private readonly todoModel: typeof TodoModel,
  ) {}

  async findAll(): Promise<TodoModel[]> {
    return await this.todoModel.query();
  }

  async findOne(id: string) {
    const todo = await this.todoModel.query().findById(+id);
    if (!todo) {
      throw new NotFoundException(`Todo #${id} not found`);
    }
    return todo;
  }

  async create(todoDto: TodoDto) {
    return await this.todoModel.query().insert(todoDto);
  }

  async update(id: string, todoDto: TodoDto) {
    const todo = await this.todoModel
      .query()
      .findById(+id)
      .patch(todoDto);
    if (!todo) {
      throw new NotFoundException(`Todo #${id} not found`);
    }
    return todo;
  }

  async remove(id: string) {
    return await this.todoModel.query().deleteById(+id);
  }
}
