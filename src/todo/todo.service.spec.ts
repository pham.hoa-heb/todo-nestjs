import { TodoDto } from './todo.dto';
import { Test, TestingModule } from '@nestjs/testing';
import { QueryBuilder } from 'objection';
import { TodoModel } from './todo.model';
import { TodoService } from './todo.service';
import { ObjectionModule } from '@willsoto/nestjs-objection';

describe('TodoService', () => {
  let todoService: TodoService;
  let TodoModelSpied: typeof TodoModel;
  let QuerySpy: jest.SpyInstance<any>;
  let moduleRef: TestingModule

  beforeEach(async () => {
    TodoModelSpied = TodoModel;
    QuerySpy = jest.spyOn(TodoModelSpied, 'query');

    moduleRef = await Test.createTestingModule({
      imports: [ObjectionModule.forFeature([TodoModel])],
      providers: [TodoService],
    })
      .overrideProvider(TodoModel)
      .useValue(TodoModelSpied)
      .compile();

    todoService = moduleRef.get<TodoService>(TodoService);
  });

  describe('findOne()', () => {
    it('should return only one todo item', async () => {
      const todoItem = todoItemFactory();

      QuerySpy.mockReturnValue(
        QueryBuilder.forClass(TodoModel).resolve(todoItem),
      );

      const queryResult = await todoService.findOne('1');
      expect(queryResult).toMatchSnapshot()
      expect(queryResult).toBe(todoItem);      
    });
  });
});

function todoItemFactory({ id, name, done }: Partial<TodoModel> = {}) {
  const todoItem = new TodoModel();
  todoItem.id = id ?? 1;
  todoItem.name = name ?? 'fake todo';
  todoItem.done = done ?? false;
  return todoItem;
}
