import { TodoModel } from './todo.model';
import { TodoService } from './todo.service';
import { Test, TestingModule } from '@nestjs/testing';
import { TodosController } from './todo.controller';
import { QueryBuilder } from 'objection';

describe('TodoController', () => {
  let todosController: TodosController;
  let todosService: TodoService;
  let todoModelSpy: jest.SpyInstance<QueryBuilder<TodoModel>>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TodoModel],
      controllers: [TodosController],
      providers: [TodoService],
    }).compile();

    todosController = module.get<TodosController>(TodosController);
    todosService = module.get<TodoService>(TodoService);
  });

  describe('findAll()', () => {
    const todoItem = new TodoModel();
    todoItem.id = 1;
    todoItem.name = 'fake todo';
    todoItem.done = false;

    todoModelSpy = jest.spyOn(TodoModel, 'query');
    todoModelSpy.mockResolvedValueOnce([todoItem]);
  });
});
