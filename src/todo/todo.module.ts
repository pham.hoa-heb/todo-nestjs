import { TodoModel } from './todo.model';
import { DatabaseModule } from './../database/database.module';
import { Module } from '@nestjs/common';
import { TodoService } from './todo.service';
import { ObjectionModule } from '@willsoto/nestjs-objection';
import { TodosController } from './todo.controller';

@Module({
  imports: [ObjectionModule.forFeature([TodoModel]), DatabaseModule],
  exports: [TodoService],
  providers: [TodoService],
  controllers: [TodosController],
})
export class TodoModule {}
