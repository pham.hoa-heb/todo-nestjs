export class TodoDto {
  name: string;
  done: boolean;
}
