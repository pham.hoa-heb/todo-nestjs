import { FilterableField } from '@nestjs-query/query-graphql';
import { Int, ObjectType } from '@nestjs/graphql';

@ObjectType('TodoGqlModel')
export class TodoGqlDto {
  @FilterableField(() => Int)
  id!: number;

  @FilterableField()
  name!: string;

  @FilterableField()
  done: boolean;
}
