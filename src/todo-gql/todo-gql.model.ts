import { Model } from 'objection';
export class TodoGqlModel extends Model {
  static tableName = 'todos_gql';

  id!: number;
  name!: string;
  done: boolean;
}
