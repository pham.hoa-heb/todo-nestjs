import { Module } from '@nestjs/common';
import { NestjsQueryObjectionModule } from '@sco/query-objection';
import { TodoGqlModel } from './todo-gql.model';
import { TodoGqlResolver } from './todo-gql.resolver';

@Module({
  imports: [NestjsQueryObjectionModule.forFeature([TodoGqlModel])],
  providers: [TodoGqlResolver],
})
export class TodoGqlModule {}
