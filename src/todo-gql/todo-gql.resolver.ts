import { TodoGqlModel } from './todo-gql.model';
import { Resolver } from '@nestjs/graphql';
import { CRUDResolver, PagingStrategies } from '@nestjs-query/query-graphql';
import { TodoGqlDto } from './todo-gql.dto';
import { InjectQueryService, QueryService } from '@nestjs-query/core';

@Resolver(() => TodoGqlDto)
export class TodoGqlResolver extends CRUDResolver(TodoGqlDto, {
  // pagingStrategy: PagingStrategies.OFFSET,
}) {
  constructor(
    @InjectQueryService(TodoGqlModel)
    readonly todoQueryService: QueryService<TodoGqlModel>,
  ) {
    super(todoQueryService);
  }
}
