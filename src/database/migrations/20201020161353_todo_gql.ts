import * as Knex from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('todos_gql', function(table) {
    table.increments();
    table.string('name');
    table.boolean('done');
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('todos_gql');
}
