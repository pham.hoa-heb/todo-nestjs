import * as Knex from 'knex';

export async function seed(knex: Knex): Promise<void> {
  await knex('todos').del();

  const todos = [
    {
      name: 'Create Todo Controller',
      done: false,
    },
    {
      name: 'Create Todo Service',
      done: false,
    },
    {
      name: 'Create Todo Model',
      done: false,
    },
  ];

  return await knex('todos').insert(todos);
}
