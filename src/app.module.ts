import { TodoModule } from './todo/todo.module';
import { GraphQLModule } from '@nestjs/graphql';
import { DatabaseModule } from './database/database.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TodoGqlModule } from './todo-gql/todo-gql.module';
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';

@Module({
  imports: [
    DatabaseModule,
    TodoModule,
    TodoGqlModule,
    GraphQLModule.forRoot({
      autoSchemaFile: true,
      path: '/api/graphql',
      playground: true,
      debug: true,
    }),
    NestjsQueryGraphQLModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
